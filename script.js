
function bannersScroll(){
    var headerHeight = $j('#header')[0].offsetHeight;
    var footerHeight = $j('.footer-container')[0].offsetHeight;
    var headerOffset = $j('#header')[0].getBoundingClientRect().top + headerHeight;
    var footerOffset = -($j('.footer-container')[0].getBoundingClientRect().bottom - $j(window).height() - footerHeight);

    if(headerOffset > -1 && headerOffset < headerHeight && footerOffset < 0){
        $j('.main-banner').each(function () {
            $j(this).css({'top': headerOffset, 'bottom': 'auto'});
        })
    } else if (headerOffset < 0 && footerOffset < 0){
        $j('.main-banner').each(function () {
            $j(this).css({'top': 0, 'bottom': 'auto'});
        })
    } else if (headerOffset < 0 && footerOffset > -1){
        $j('.main-banner').each(function () {
            $j(this).css({'top': 'auto', 'bottom': footerOffset});
        })
    } else {
        $j('.main-banner').each(function () {
            $j(this).css({'top': headerHeight, 'bottom': 'auto'});
        })
    }
}

$j(window).on('load', function () {
    var headerHeight = $j('#header')[0].offsetHeight;
    var footerHeight = $j('.footer-container')[0].offsetHeight;
    var bannerHeight = $j('.main-banner').height();
    var documentHeight = $j( document ).height();

    if(documentHeight > (headerHeight + footerHeight + bannerHeight)){
        bannersScroll();
        $j('.main-banner').each(function () {
            $j(this).animate({ opacity: 1 });
        })
	}else{
        $j('.col1-layout .col-main').animate({'padding-top': 100});
        $j('.main-container').animate({'min-height': bannerHeight});
        $j('.main-banner').each(function () {
            $j(this).animate({ opacity: 1, top: headerHeight});
        })
	}

    $j('.main-banner').each(function () {
        $j(this).fadeIn("fast");
    });
});

$j(window).on('scroll', function () {
    bannersScroll();
});